package ru.t1.simanov.tm.exception.user;

public final class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

    public RoleEmptyException(String message) {
        super(message);
    }

    public RoleEmptyException(String message, Throwable cause) {
        super(message, cause);
    }

    public RoleEmptyException(Throwable cause) {
        super(cause);
    }

    public RoleEmptyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
