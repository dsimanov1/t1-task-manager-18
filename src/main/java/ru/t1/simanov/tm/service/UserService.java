package ru.t1.simanov.tm.service;

import ru.t1.simanov.tm.api.repository.IUserRepository;
import ru.t1.simanov.tm.api.service.IUserService;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.exception.entity.UserNotFoundException;
import ru.t1.simanov.tm.exception.field.IdEmptyException;
import ru.t1.simanov.tm.exception.field.LoginEmptyException;
import ru.t1.simanov.tm.exception.field.PasswordEmptyException;
import ru.t1.simanov.tm.exception.user.ExistsEmailException;
import ru.t1.simanov.tm.exception.user.ExistsLoginException;
import ru.t1.simanov.tm.exception.user.RoleEmptyException;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return userRepository.create(login, password);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(login)) throw new ExistsEmailException();
        return userRepository.create(login, password, email);
    }

    @Override
    public User create(String login, String password, javax.management.relation.Role role) {
        return null;
    }


    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        return userRepository.create(login, password, role);
    }

    @Override
    public User add(final User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) return null;
        return userRepository.findByEmail(email);
    }

    @Override
    public User remove(final User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User removeById(String id) {
        return null;
    }

    @Override
    public User removeByLogin(String login) {
        return null;
    }

    @Override
    public User removeByEmail(String email) {
        return null;
    }

    @Override
    public User setPassword(String id, String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExists(final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isLoginExists(login);
    }

    @Override
    public Boolean isEmailExists(String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.isEmailExists(email);
    }

}
