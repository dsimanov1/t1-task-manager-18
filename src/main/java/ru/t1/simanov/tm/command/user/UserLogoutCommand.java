package ru.t1.simanov.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Logout current user.";

    public static final String NAME = "logout";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
