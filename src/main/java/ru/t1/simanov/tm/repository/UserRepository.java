package ru.t1.simanov.tm.repository;

import ru.t1.simanov.tm.api.repository.IUserRepository;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    public User create(final String login, final String password, final String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    public User add(final User user) {
        users.add(user);
        return user;
    }

    public List<User> findAll() {
        return users;
    }

    public User findById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    public User findByEmail(final String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public Boolean isLoginExists(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExists(final String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

}
